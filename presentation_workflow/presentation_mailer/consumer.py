import json
import pika
import django
import os
import sys
from django.core.mail import send_mail
from pika.exceptions import AMQPConnectionError
import time


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

while True:
    try:
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(host="rabbitmq")
        )
        channel = connection.channel()
        channel.queue_declare(queue="presentation_approval")
        channel.queue_declare(queue="presentation_rejection")

        def reject(ch, method, properties, body):
            data = json.loads(body)
            email = data["presenter_email"]
            name = data["presenter_name"]
            title = data["title"]
            send_mail(
                "Your presentation has been rejected",
                f"We're sorry, {name}, but your presentation {title} has been rejected",
                "admin@conferece.go",
                [email],
                fail_silently=False,
            )

        def process_approval(ch, method, properties, body):
            content = json.loads(body)

            send_mail(
                "Your presentation has been accepted",
                f"{content['presenter_name']},  {content['title']} has been accepted",
                "admin@conference.go",
                [content["presenter_email"]],
                fail_silently=False,
            )

        channel.basic_consume(
            queue="presentation_approval",
            on_message_callback=process_approval,
            auto_ack=True,
        )
        channel.basic_consume(
            queue="presentation_rejection",
            on_message_callback=reject,
            auto_ack=True,
        )

        channel.start_consuming()

    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
