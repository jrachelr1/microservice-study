from django.db import models


class AccountVO(models.Model):
    email = models.CharField(max_length=100)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    is_active = models.BooleanField(null=True, blank=True)
    updated = models.DateTimeField()
